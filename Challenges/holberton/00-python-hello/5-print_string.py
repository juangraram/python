#!/usr/bin/python3
"""
The output of the program should be:
3 times the value of str
followed by a new line
followed by the 9 first characters of str
followed by a new line
You are not allowed to use any loops or conditional statement
Your program should be maximum 5 lines long
"""

str = "New York"
print(3 * str)
print(str[0:9])
