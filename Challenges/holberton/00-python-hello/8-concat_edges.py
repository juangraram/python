#!/usr/bin/python3
"""
You are not allowed to use any loops or conditional statements
Your program should be exactly 5 lines long
You are not allowed to create new variables
You are not allowed to use string literals
"""
str = "Python is an interpreted, interactive, object-oriented programming\
language that combines remarkable power with very clear syntax"
str = str[39:66] + ' ' + str[106:110] + ' ' + str[0:6]
print(str)

