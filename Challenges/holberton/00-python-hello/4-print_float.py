#!/usr/bin/python3
"""
The output of the program should be:
Float:, followed by the float with only 2 digits
followed by a new line
You are not allowed to cast number to string
You have to use the new print formatting tips (with .format(...))
"""

number = 3.14159

print("Float: {0:.2f}".format(number))
