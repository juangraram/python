#!/usr/bin/python3
"""
The output of the script should be:
the number, followed by Battery street,
followed by a new line
You are not allowed to cast the variable number into a string
Your code must be 3 lines long
You have to use the new print numbers tips (with .format(...))
Output:
98 Battery street
"""
number = 98
print("{} Battery street".format(number))
