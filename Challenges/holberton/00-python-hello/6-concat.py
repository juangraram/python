#!/usr/bin/python3
"""
You are not allowed to use any loops or conditional statements.
You have to use the variables str1 and str2 in your new line of code
Your program should be exactly 5 lines long
"""

str1 = "New"
str2 = "York"
str1 = str1 +" " + str2
print("Welcome to {}!".format(str1))
