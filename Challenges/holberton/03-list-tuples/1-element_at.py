#!/usr/bin/python3

"""
Write a function that retrieves an element from a list.
Prototype: def element_at(my_list, idx):
If idx is negative, the function should return None
If idx is out of range (> of number of element in my_list), the function should return None
"""


def element_at(my_list, idx):
    if idx > len(my_list):
        return None
    elif idx < 0:
        return None
    else:
        return my_list[idx]
