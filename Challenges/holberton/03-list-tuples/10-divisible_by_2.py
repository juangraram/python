#!/usr/bin/python3

"""
Write a function that finds all multiples of 2 in a list.
Prototype: def divisible_by_2(my_list=[]):
Return a new list with True or False, depending on whether the integer
at the same position in the original list is a multiple of 2
The new list should have the same size as the original list
You are not allowed to import any module
Output:
0 is divisible by 2
1 is not divisible by 2
2 is divisible by 2
3 is not divisible by 2
4 is divisible by 2
5 is not divisible by 2
6 is divisible by 2
"""


def divisible_by_2(my_list=[]):
    new_list = []
    for num in my_list:
        if num % 2 == 0:
            new_list.append(True)
        else:
            new_list.append(False)
    return new_list
