#!/usr/bin/python3

"""
Write a function that finds the biggest integer of a list.
Prototype: def max_integer(my_list=[]):
If the list is empty, return None
You can assume that the list only contains integers
You are not allowed to import any module
You are not allowed to use the builtin max()
Output:
Max: 90
"""


def max_integer(my_list=[]):
    # Two differents forms to do this task
    # One
    if my_list == 0:
        return None
    else:
        my_list.sort()
        return my_list[-1]

    # Two
    numMax = max(my_list)
    return numMax
