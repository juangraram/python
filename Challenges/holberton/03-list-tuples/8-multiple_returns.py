#!/usr/bin/python3

"""
Write a function that returns the length of a string and its first character.
Prototype: def multiple_returns(sentence):
If the sentence is empty, the first character should be equal to None
You are not allowed to import any module
Output:
Length: 32 - First character: A
"""


def multiple_returns(sentence):
    if len(sentence) == 0:
        return 0, None
    else:
        return len(sentence), sentence[0]
