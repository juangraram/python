#!/usr/bin/python3

"""
Write a function that prints all integers of a list, in reverse order.
Prototype: def print_reversed_list_integer(my_list=[]):
Format: one integer per line. See example
You are not allowed to import any module
You can assume that the list only contains integers
You are not allowed to cast integers into strings
You have to use str.format() to print integers
"""


def print_reversed_list_integer(my_list=[]):
    # Form 1
    for i in my_list[::-1]:
        print(i)
    # Form 2
    for num in reversed(my_list):
        print(num)
    # Form 3
    my_list.reverse()
    for j in my_list:
        print(j)

    print(my_list, end="")
