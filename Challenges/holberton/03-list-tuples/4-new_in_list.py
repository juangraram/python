#!/usr/bin/python3

"""
Write a function that replaces an element in a list at a specific position without modifying the original list.
Prototype: def new_in_list(my_list, idx, element):
If idx is negative, the function should return a copy of the original list
If idx is out of range (> of number of element in my_list), the function should return a copy of the original list
You are not allowed to import any module
You are not allowed to use try/except
Output:
[1, 2, 3, 9, 5]
[1, 2, 3, 4, 5]
"""


def new_in_list(my_list, idx, element):
    # new_list = my_list.copy()
    # new_list = my_list[:]
    new_list = list(my_list)
    if idx < 0:
        return my_list
    elif idx > len(my_list):
        return my_list
    else:
        new_list[idx] = element
        return new_list
