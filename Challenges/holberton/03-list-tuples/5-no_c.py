#!/usr/bin/python3

"""
Write a function that removes all characters c and C from a string.
Prototype: def no_c(my_string):
The function should return the new string
You are not allowed to import any module
You are not allowed to use str.replace()
Output:
Holberton Shool
hiago
 is fun!
"""


def no_c(my_string):
    new = ""
    for letter in my_string:
        if letter != 'c' and letter != 'C':
            # new = new + letter
            new += letter
    return new

    for letter in my_string:
        if letter not in 'Cc':
            new = new + letter
    return new
