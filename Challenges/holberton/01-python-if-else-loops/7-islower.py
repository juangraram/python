#!/usr/bin/env python3
"""
Write a function that checks for lowercase character.
Prototype: def islower(c):
Returns True if c is lowercase
Returns False otherwise
You are not allowed to import any module
You are not allowed to use str.upper() and str.isupper()
https://docs.python.org/3/library/functions.html#ord
"""

def islower(c):
    letter = ord(c)
    if letter >= 97 and letter <= 122:
        return True
    else:
        return False
