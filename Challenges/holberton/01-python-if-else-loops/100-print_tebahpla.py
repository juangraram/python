#!/usr/bin/python3
"""
Write a program that prints the ASCII alphabet, in reverse order, alternating lowercase and uppercase (z in lowercase and Y in uppercase) , not followed by a new line.
You can only use one print function with string format
You can only use one loop in your code
You are not allowed to store characters in a variable
You are not allowed to import any module
"""


for letter in range(122, 96, -1):
    if letter % 2 == 1:
        letter = letter - 32
    print("{}".format(chr(letter)), end="")
