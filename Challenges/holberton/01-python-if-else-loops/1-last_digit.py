#!/usr/bin/python3
"""
The variable number will store a different value every time you will run this program
You don’t have to understand what import, random.randint do. Please do not touch this code. This line should not change: number = random.randint(-10000, 10000)
The output of the program should be:
The string Last digit of, followed by
the number, followed by
the string is, followed by the last digit of number, followed by
if the last digit is greater than 5: the string and is greater than 5
if the last digit is 0: the string and is 0
if the last digit is less than 6 and not 0: the string and is less than 6 and not 0
followed by a new line
"""

import random
number = random.randint(-10000, 10000)
lastDigit = abs(number) % 10 # with the function abs returns the absolute value of the given number Absolute value of a number is the value without considering its sign
if lastDigit > 5:
    print("Last digit of {:d} is {:d} greater than 5".format(number, lastDigit))
elif lastDigit == 0:
    print("Last digit of {:d} is {:d} is 0 ".format(number, lastDigit))
else:
    print("Last digit of {:d} is {:d} less than 6 and not 0".format(number, lastDigit))
