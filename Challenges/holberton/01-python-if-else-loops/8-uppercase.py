#!/usr/bin/python3
"""
Write a function that prints a string in uppercase followed by a new line.
Prototype: def uppercase(str):
You can only use no more than 2 print functions with string format
You can only use one loop in your code
You are not allowed to import any module
You are not allowed to use str.upper() and str.isupper()
Tips: ord(https://docs.python.org/3/library/functions.html#ord)
"""


def uppercase(str):
    for letter in str:
        if letter >= 'a' and letter <= 'z':
            letter = chr(ord(letter) - 32)
        print("{}".format(letter), end='')
    print()