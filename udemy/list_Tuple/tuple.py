#!/usr/bin/python3

"""
Learn how to use tuple
"""

# D efinir una tupla
frutas = ('Naranja', 'Plátano', 'Guayaba')
print(frutas)
# saber el largo
print(len(frutas))
# acceder a un elemento
print(frutas[0])
# navegación inversa
print(frutas[-1])
# acceder a un rango
print(frutas[0:1])  # sin incluir el último índice
# recorrer elementos
for fruta in frutas:
    print(fruta, end=' ')
# cambiar valor tupla
# frutas[0] = 'Pera'
frutasLista = list(frutas)
frutasLista[0] = 'Pera'
frutas = tuple(frutasLista)
print('\n', frutas)
# eliminar la tupla
del frutas

# Add tuples

tuple_a = (1, 89, 5)
tuple_b = (88, 11, 7)

res = tuple(map(sum, zip(tuple_a, tuple_b)))
print(res)

# Convert tuple in a list

tupla = (13, 1, 8, 3, 2, 5, 8)
print(tupla)
listas=[]
for num in tupla:
    if num <= 5:
        listas.append(num)
        print(num)
print(listas)
