# Define a function
def my_string():
    print('saludos desde mi función')


my_string()


# Define a function with parameters and pass arguments
def my_function(name, lastname):
    print(f'Nombre: {name}, Apellido: {lastname}')


my_function('Juan', 'Perez')
my_function('Karla', 'Lara')


# Use the word return
def add(a, b):
    return a + b


result = add(5, 3)


print(f'Add result: {result}')
print(f'Add result: {add(5,3)}')

# Define a function with parameters (*args) and pass arguments


def listnames(*args):
    for name in args:
        print(name)


listnames('Karla', 'Juan', 'Pedro')

# Define a function with parameters (**kwargs) and pass arguments


def listTerm(**terms):
    for key, value in terms.items():
        print(f'{key}: {value}')

listTerm(IDE='Integrated Developement Environment', PK='Primary Key')
listTerm(DBMS='Database Management System')

# Function recursive
# 5! = 5 * 4 * 3 * 2 * 1
# 5! = 5 * 4 * 3 * 2
# 5! = 5 * 4 * 6
# 5! = 5 * 24
# 5! = 120


def factorial(number):
    if number == 1:
        return 1
    else:
        return number * factorial(number - 1)

number = 5
result = factorial(number)
print(f'El factorial de {number} es {result}')

# Functions with different types


def desplegarNombres(names):
    for name in names:
        print(name)


names = ['Juan', 'Karla', 'Guillermo']
desplegarNombres(names)
desplegarNombres('Carlos')
desplegarNombres((8, 9))
desplegarNombres([10, 11])
