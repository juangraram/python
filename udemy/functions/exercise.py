# Add


def add(*nums):
    result = 0
    for num in nums:
        result += num
    return result

# Call the function


print(add(3, 5, 9))

# Multiply


def mul(*mult):
    result = 1
    for numbers in mult:
        result *= numbers
    return result


print(mul(5, 6, 8))

# Divide


def div(*divi):
    result1 = 1
    for numbers1 in divi:
        result1 /= numbers1
    return result1


print(div(5, 6, 8))


# Function recursive


def reverse_recursive(number):
    if number >= 1:
        print(number)
        reverse_recursive(number - 1)


reverse_recursive(5)
