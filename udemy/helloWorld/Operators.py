#!/usr/bin/python3

"""
Learn operators
"""

# Arithmetics operators
num1 = 3
num2 = 2
add = num1 + num2

print('Result of the add: ', add)
# With 'f' it's know how interpolation
print(f'Result of the add: {add} ')

substraction = num1 - num2
print(f'Result substraction: {substraction} ')

multiplication = num1 * num2
print(f'Resultado multiplicación: {multiplication}')

division = num1 / num2
print(f'Resultado división: {division}')

division = num1 // num2
print(f'Resultado división (int): {division}')

module = num1 % num2
print(f'Resultado residuo división (módulo): {module}')

exponente = num1 ** num2
print(f'Resultado exponente: {exponente}')

# Asignation Operators

miVariable = 10
print(miVariable)

miVariable = miVariable + 1
print(miVariable)
# incremento con reasignación
miVariable += 1
print(miVariable)
# miVariable = miVariable - 2
miVariable -= 2
print(miVariable)
# miVariable = miVariable * 3
miVariable *= 3
print(miVariable)

miVariable /= 2
print(miVariable)

"""
 Comparation operators
"""

a = 4
b = 6

resultado = a == b
print(f'Resultado == : {resultado}')

resultado = a != b
print(f'Resultado != : {resultado}')

resultado = a > b
print(f'Resultado > : {resultado}')

resultado = a >= b
print(f'Resultado >= : {resultado}')

resultado = a < b
print(f'Resultado < : {resultado}')

resultado = a <= b
print(f'Resultado: <= {resultado}')