#!/usr/bin/python3
"""
Python data types
Numerics - Strings - Boolean
"""

# Type Int
num = 10
print(num)
print(type(num))

# Type float
num = 1.53
print(num)
print(type(num))

# Type Boolean
num = True
print(num)
print(type(num))

# Type String
num = "Hi classmate"
print(num)
print(type(num))
