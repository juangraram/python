#!/usr/bin/python3

"""
Learn how to input information by function input
"""

# To convert string input to int input 
num1 = int(input('Write number one: '))
num2 = int(input('Write number two: '))
result = num1 + num2
print("The result of the add is:", result)
