#!/usr/bin/python3
"""
Learn about variables
"""

name = "Hi python's"
print(name)

name = "Pedro"
print(name)

num1 = 2
num2 = 5
print(num1+num2)

num1 = 2
num2 = 5
result = num1 + num2
print(result)

# Print position in memory

print(id(num1))
print(id(num2))
print(id(result))
