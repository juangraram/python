#!/usr/bin/python3

"""
Lear how to use type bool
"""
num = False
print(num)

num = 3 > 2
print(num)

if num:
    print("The result was true")
else:
    print("The result was false")
