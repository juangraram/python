#!/usr/bin/python3

"""
Learn how to concat two variables
"""

name = 'Juan' + " " + 'Fernando'
lastname = 'Granada' + " " + 'Ramirez'
print(name + " " + lastname)
print(name, lastname)

# Concat number
num1 = "1"
num2 = "2"
print(num1 + num2)
print(num1, num2)

# Convert string to int
print('Add', int(num1) + int(num2))
