#!/usr/bin/python3

"""
Learn the sentence if else
"""


condicion = True

if condicion == True:
    print('Condición verdadera')
elif condicion == False:
    print('Condición falsa')
else:
    print('Condición no reconocida')