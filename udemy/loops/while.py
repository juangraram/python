#!/usr/bin/python3

"""
Learn loops while
"""

"""
condicion = True

while condicion:
    print('Ejecutando ciclo while infinito')
else:
    print('Fin del ciclo while')
"""


contador = 0
while contador < 3:
    print(contador)
    contador += 1
else:
    print('Fin ciclo while')
