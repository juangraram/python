#!/usr/bin/python3

"""
Learn loops for
"""

cadena = "Hola"

for i in cadena:
    print(i)

####################

for number in range(10):
    print(number)

for letter in 'Holanda':
    if letter == 'a':
        print(f'Letter is: {letter}')
        break  # Romper con el ciclo, es util en lista de datos
    else:
        print('End loops')


for num in range(0, 10):
    if num % 2 == 0:
        print(f'Its odd: {num}')
